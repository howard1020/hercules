package api

import (
	"net/http"
	"runtime"
	"time"

	"../convert"
	"../server"
	"../snapshot"
	"../tangle"
	"../tangle/tanglesync"
	"../utils"
	"github.com/gin-gonic/gin"
)

func init() {
	addAPICall("getNodeInfo", getNodeInfo, mainAPICalls)
}

func getNodeInfo(request Request, c *gin.Context, ts time.Time) {
	var stats runtime.MemStats
	runtime.ReadMemStats(&stats)

	server.NeighborsLock.RLock()
	neighborsCnt := len(server.Neighbors)
	server.NeighborsLock.RUnlock()

	tipsCnt := tangle.TipsCache.EntryCount()

	milestone := tangle.GetLatestMilestone()
	milestoneHash := convert.BytesToTrytes(milestone.TX.Hash)[:81]
	solid := dummyHash
	sindex := 500000
	isTangleSynced := tanglesync.IsSynchronized()
	if isTangleSynced {
		solid = milestoneHash
		sindex = milestone.Index
	}
	c.JSON(http.StatusOK, gin.H{
		"appName":                            "Deviota Hercules Go",
		"appVersion":                         "0.1.0",
		"availableProcessors":                runtime.NumCPU(),
		"currentRoutines":                    runtime.NumGoroutine(),
		"allocatedMemory":                    stats.Sys,
		"latestMilestone":                    milestoneHash,
		"latestMilestoneIndex":               milestone.Index,
		"latestSolidSubtangleMilestone":      solid,
		"latestSolidSubtangleMilestoneIndex": sindex,
		"neighbors":                          neighborsCnt,
		"currentSnapshotTimestamp":           snapshot.CurrentTimestamp,
		"currentSnapshotTimeHumanReadable":   utils.GetHumanReadableTime(snapshot.CurrentTimestamp),
		"isSynchronized":                     isTangleSynced,
		"tips":                               tipsCnt,
		"time":                               time.Now().Unix(),
		"duration":                           getDuration(ts),
	})
}
