package networkutils

import "strings"

type IPAddress string

func (ip *IPAddress) IsIPv6() bool {
	return len(strings.Split(string(*ip), ":")) > 1
}

func (ip *IPAddress) ToString() string {
	if ip.IsIPv6() {
		// IPv6
		return "[" + string(*ip) + "]"
	}

	return string(*ip)
}

func GetPreferredIP(ipAddressSlice []*IPAddress, preferIPv6 bool) string {
	for _, ip := range ipAddressSlice {
		if ip.IsIPv6() == preferIPv6 {
			return ip.ToString() // Returns IPv4 if IPv6 is not preferred, or IPv6 if otherwise
		}
	}

	if len(ipAddressSlice) > 0 {
		return ipAddressSlice[0].ToString() // Returns first IP if nothing preferred was found
	}

	return ""
}

func GetFormattedAddress(identifier string, port string) string {
	return identifier + ":" + port
}
