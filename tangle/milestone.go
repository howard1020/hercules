package tangle

import (
	"bufio"
	"bytes"
	"io"
	"os"
	"strings"
	"sync"
	"time"

	"../convert"
	"../db"
	"../db/coding"
	"../db/ns"
	"../logs"
	"../transaction"
)

type Milestone struct {
	TX    *transaction.FastTX
	Index int
}

type PendingMilestone struct {
	Key         []byte
	TX2BytesKey []byte
	LastCheck   time.Time
}

const (
	COO_ADDRESS  = "KPWCHICGJZXKE9GSUDXZYUAPLHAKAHYHDXNPHENTERYMMBQOPSQIDENXKLKCEYCPVTZQLEEJVYJZV9BWU"
	COO_ADDRESS2 = "999999999999999999999999999999999999999999999999999999999999999999999999999999999"

	milestoneTickerInterval = time.Duration(2) * time.Second
	milestoneCheckInterval  = time.Duration(5) * time.Second
)

var (
	COO_ADDRESS_BYTES         = convert.TrytesToBytes(COO_ADDRESS)[:49]
	COO_ADDRESS2_BYTES        = convert.TrytesToBytes(COO_ADDRESS2)[:49]
	pendingMilestoneQueue     = make(chan *PendingMilestone, maxQueueSize)
	pendingMilestoneWaitGroup = &sync.WaitGroup{}
	pendingMilestoneQueueQuit = make(chan struct{})
	pendingMilestoneTicker    *time.Ticker
	latestMilestone           *Milestone // TODO: Access latest milestone only via GetLatestMilestone()
	LatestMilestoneLock       = &sync.RWMutex{}
	lastMilestoneCheck        = time.Now()
)

// TODO: remove this? Or add an API interface?
func LoadMissingMilestonesFromFile(path string) error {
	logs.Log.Info("Loading missing...")
	total := 0
	f, err := os.OpenFile(path, os.O_RDONLY, os.ModePerm)
	if err != nil {
		logs.Log.Fatalf("open file error: %v", err)
		return err
	}
	defer f.Close()

	rd := bufio.NewReader(f)
	for {
		line, err := rd.ReadString('\n')
		if err != nil {
			if err == io.EOF {
				break
			}
			logs.Log.Fatalf("read file line error: %v", err)
			return err
		}
		line = strings.TrimSpace(line)
		hash := convert.TrytesToBytes(line)
		if len(hash) < 49 {
			continue
		}
		hash = hash[:49]

		dbTx := db.Singleton.NewTransaction(true)
		exists := requestIfMissing(hash, true, dbTx)
		dbTx.Commit()

		if err == nil {
			if !exists {
				//logs.Log.Warning("MISSING", line)
				total++
			} else {
				key := ns.HashKey(hash, ns.NamespaceMilestone)
				if !db.Singleton.HasKey(key) {
					bits, err := db.Singleton.GetBytes(ns.Key(key, ns.NamespaceBytes))
					if err != nil {
						logs.Log.Error("Couldn't get milestonetx bytes", err)
						continue
					}
					trits := convert.BytesToTrits(bits)[:8019]
					tx := transaction.TritsToTX(&trits, bits)
					trunkBytesKey := ns.HashKey(tx.TrunkTransaction, ns.NamespaceBytes)
					err = db.Singleton.PutBytes(ns.Key(key, ns.NamespaceEventMilestonePending), trunkBytesKey)
					pendingMilestone := &PendingMilestone{Key: key, TX2BytesKey: trunkBytesKey, LastCheck: time.Now().Add(-milestoneCheckInterval)}
					logs.Log.Debugf("Added missing milestone: %v", convert.BytesToTrytes(tx.Hash)[:81])
					addPendingMilestoneToQueue(pendingMilestone)
				}
			}
		} else {
			logs.Log.Error("ERR", err)
		}
	}
	logs.Log.Info("Loaded missing", total)

	return nil
}

func milestoneOnLoad() {
	logs.Log.Info("Loading milestones")

	loadLatestMilestoneFromDB()
	loadPendingMilestonesFromDB()

	go processPendingMilestones()
}

func loadPendingMilestonesFromDB() {

	logs.Log.Info("Loading pending milestones from DB")

	confirmedCnt := 0
	pendingCnt := 0

	var keysToRemove [][]byte

	// Get all pending milestones
	db.Singleton.Update(func(dbTx db.Transaction) error {
		return ns.ForNamespace(dbTx, ns.NamespaceEventMilestonePending, true, func(key, tx2BytesKey []byte) (bool, error) {

			pending, _ := preCheckMilestone(key, tx2BytesKey, dbTx)
			if pending {
				addPendingMilestoneToQueue(&PendingMilestone{Key: key, TX2BytesKey: tx2BytesKey, LastCheck: time.Now().Add(-milestoneCheckInterval)})
				pendingCnt++
			} else {
				confirmedCnt++
				keysToRemove = append(keysToRemove, key)
			}

			return true, nil
		})
	})

	db.Singleton.Update(func(dbTx db.Transaction) (err error) {
		for _, key := range keysToRemove {
			dbTx.Remove(key) // NamespaceEventMilestonePending
		}
		return nil
	})

	logs.Log.Infof("Pending milestones loaded. Confirmed: %v, Pending: %v", confirmedCnt, pendingCnt)
}

func loadLatestMilestoneFromDB() {
	logs.Log.Infof("Loading latest milestone from database...")
	err := db.Singleton.View(func(dbTx db.Transaction) error {
		var latestKey []byte
		latestIndex := 0

		setLatestMilestonePointer(&Milestone{transaction.TipFastTX, latestIndex})

		err := coding.ForPrefixInt(dbTx, ns.Prefix(ns.NamespaceMilestone), true, func(key []byte, index int) (bool, error) {
			if index >= latestIndex {
				latestKey = ns.Key(key, ns.NamespaceBytes)
				latestIndex = index
			}
			return true, nil
		})
		if err != nil {
			return err
		}

		if len(latestKey) == 0 {
			// No milestone found in database => started from snapshot
			return nil
		}

		txBytes, err := dbTx.GetBytes(latestKey)
		if err != nil {
			return err
		}

		trits := convert.BytesToTrits(txBytes)[:8019]
		tx := transaction.TritsToTX(&trits, txBytes)

		setLatestMilestonePointer(&Milestone{tx, latestIndex})

		return nil
	})
	if err != nil {
		logs.Log.Errorf("Loaded latest milestone from database failed! Err: %v", err)
	} else {
		milestone := GetLatestMilestone()
		logs.Log.Infof("Loaded latest milestone from database: %v", milestone.Index)
	}
}

func GetLatestMilestone() Milestone {
	LatestMilestoneLock.RLock()
	defer LatestMilestoneLock.RUnlock()
	return *latestMilestone
}

func setLatestMilestonePointer(milestone *Milestone) {
	LatestMilestoneLock.Lock()
	defer LatestMilestoneLock.Unlock()
	latestMilestone = milestone
}

func setLatestMilestone(newIndex int, tx *transaction.FastTX) (changed bool) {
	milestone := GetLatestMilestone()
	if milestone.Index < newIndex {
		// Conversion to get the Hashes
		trits := convert.BytesToTrits(tx.Bytes)[:8019]
		fastTx := transaction.TritsToTX(&trits, tx.Bytes)

		setLatestMilestonePointer(&Milestone{fastTx, newIndex})

		logs.Log.Infof("Latest milestone changed to: %v", newIndex)
		return true
	}
	return false
}

func addPendingMilestoneToQueue(pendingMilestone *PendingMilestone) {
	pendingMilestoneQueue <- pendingMilestone
}

func processPendingMilestones() {
	pendingMilestoneWaitGroup.Add(1)
	defer pendingMilestoneWaitGroup.Done()

	pendingMilestoneTicker = time.NewTicker(milestoneTickerInterval)
	for {
		select {
		case <-pendingMilestoneQueueQuit:
			return

		case <-pendingMilestoneTicker.C:
			if ended {
				break
			}
			processPendingMilestonesQueue()
		}
	}
}

func processPendingMilestonesQueue() {
	/*
		How do milestones work:

		- An unknown Tx is incoming and is checked for COO address => IsMaybeMilestone
		- If it is maybe a milestone, the PendingMilestone is added to the pendingMilestoneQueue and marked in the DB with NamespaceEventMilestonePending
		- A ticker checks all elements in the queue if the second part of the milestone was received already
		- If both parts are received, the structure and the signature of the milestone is checked
		- If invalid, the pending milestone is dropped
		- If valid, the latest milestone is set and the referenced Transactions are confirmed => addPendingConfirmation
	*/

	seenMilestones := make(map[string]*struct{})
	for {
		select {

		case pendingMilestone := <-pendingMilestoneQueue:
			if ended {
				return
			}

			hash := string(pendingMilestone.Key)

			_, seenAlready := seenMilestones[hash]
			if seenAlready {
				// We are back at the start of the queue => quit loop
				// Re-add it to the queue (TODO non-blocking write?)
				pendingMilestoneQueue <- pendingMilestone
				return
			}
			seenMilestones[hash] = nil // mark pendingMilestone as seen

			if time.Now().Sub(pendingMilestone.LastCheck) < milestoneCheckInterval {
				// Do not check at every tick since it could take longer than a single tick
				pendingMilestoneQueue <- pendingMilestone
				continue
			}

			db.Singleton.Update(func(dbTx db.Transaction) (e error) {
				pending, _ := preCheckMilestone(pendingMilestone.Key, pendingMilestone.TX2BytesKey, dbTx)
				if pending {
					// Milestone check is still pending => readd it to the queue (TODO non-blocking write?)
					pendingMilestone.LastCheck = time.Now()
					pendingMilestoneQueue <- pendingMilestone
				}
				return nil
			})

		default:
			// Non blocking read is essential here, otherwise it causes a deadlock if the queue is empty
			return
		}
	}
}

func preCheckMilestone(key []byte, TX2BytesKey []byte, dbTx db.Transaction) (pending bool, valid bool) {

	// Check if 1-index TX already exists
	tx2Bytes, err := dbTx.GetBytes(TX2BytesKey)
	if err != nil {
		return true, false
	}

	// Check that the 0-index TX also exists.
	txBytesKey := ns.Key(key, ns.NamespaceBytes)
	txBytes, err := dbTx.GetBytes(txBytesKey)
	if err != nil {
		// The 0-index milestone TX doesn't exist. Clearly an error here!
		logs.Log.Panicf("A milestone has disappeared: %v", txBytesKey)
	}

	// Get TX objects and validate
	trits := convert.BytesToTrits(txBytes)[:8019]
	tx := transaction.TritsToFastTX(&trits, txBytes)

	trits2 := convert.BytesToTrits(tx2Bytes)[:8019]
	tx2 := transaction.TritsToFastTX(&trits2, tx2Bytes)

	valid = checkMilestone(txBytesKey, tx, tx2, trits2, dbTx)
	return false, valid
}

func checkMilestone(key []byte, tx *transaction.FastTX, tx2 *transaction.FastTX, trits []int, dbTx db.Transaction) (valid bool) {
	key = ns.Key(key, ns.NamespaceEventMilestonePending)

	removePendingMilestoneKeyFromDb := func() {
		err := dbTx.Remove(key)
		if err != nil {
			logs.Log.Panicf("Could not remove pending milestone: %v", err)
		}
	}

	// Get the milestone index
	milestoneIndex := getMilestoneIndex(tx)
	if (milestoneIndex < 0) || (milestoneIndex >= 0x200000) {
		logs.Log.Warningf("Milestone index out of range: %v, Milestone: %v", milestoneIndex, convert.BytesToTrytes(tx.Bundle)[:81])
		removePendingMilestoneKeyFromDb()
		return false
	}

	// Check if milestone index is already in the database
	milestoneIndexDB, err := coding.GetInt(dbTx, ns.Key(key, ns.NamespaceMilestone))
	if err == nil && (milestoneIndex == milestoneIndexDB) {
		// Already stored in the DB, no need to go on
		removePendingMilestoneKeyFromDb()
		setLatestMilestone(milestoneIndex, tx)
		return true
	}

	// Verify correct bundle structure
	if !bytes.Equal(tx2.Address, COO_ADDRESS2_BYTES) ||
		!bytes.Equal(tx2.TrunkTransaction, tx.BranchTransaction) ||
		!bytes.Equal(tx2.Bundle, tx.Bundle) {
		logs.Log.Warning("Milestone bundle verification failed for:\n ", convert.BytesToTrytes(tx.Bundle)[:81])
		removePendingMilestoneKeyFromDb()
		return false
	}

	// Verify milestone signature
	valid = validateMilestone(tx, trits, milestoneIndex)
	if !valid {
		logs.Log.Warning("Milestone signature verification failed for: ", convert.BytesToTrytes(tx.Bundle)[:81])
		removePendingMilestoneKeyFromDb()
		return false
	}

	// Save milestone, remove pending and update latest milestone
	err = coding.PutInt(dbTx, ns.Key(key, ns.NamespaceMilestone), milestoneIndex)
	if err != nil {
		logs.Log.Panicf("Could not save milestone: %v", err)
	}

	removePendingMilestoneKeyFromDb()
	setLatestMilestone(milestoneIndex, tx)

	// Trigger confirmations
	err = addPendingConfirmation(key, int64(tx.Timestamp), dbTx)
	if err != nil {
		logs.Log.Panicf("Could not save pending confirmation: %v", err)
	}

	return true
}

// Returns Milestone index if the milestone
func getMilestoneIndex(tx *transaction.FastTX) (milestoneIndex int) {
	return int(convert.TritsToInt(convert.BytesToTrits(tx.ObsoleteTag[:5])).Uint64())
}

// Validates if the milestone has the correct signature
// Params: tx + trits of the second transaction in the milestone bundle.
func validateMilestone(tx *transaction.FastTX, trits []int, milestoneIndex int) (valid bool) {
	trunkTransactionTrits := convert.BytesToTrits(tx.TrunkTransaction)[:243]
	normalized := transaction.NormalizedBundle(trunkTransactionTrits)[:transaction.NUMBER_OF_FRAGMENT_CHUNKS]
	digests := transaction.Digest(normalized, tx.SignatureMessageFragment, 0, 0, false)
	address := transaction.Address(digests)
	merkleRoot := transaction.GetMerkleRoot(
		address,
		trits,
		0,
		milestoneIndex,
		transaction.NUMBER_OF_KEYS_IN_MILESTONE,
	)
	merkleAddress := convert.TritsToTrytes(merkleRoot)
	return merkleAddress == COO_ADDRESS
}

func isMaybeMilestone(tx *transaction.FastTX) bool {
	return tx.Value == 0 && bytes.Equal(tx.Address, COO_ADDRESS_BYTES)
}

func isMaybeMilestonePair(tx *transaction.FastTX) bool {
	return tx.Value == 0 && bytes.Equal(tx.Address, COO_ADDRESS2_BYTES)
}

func isMaybeMilestonePart(tx *transaction.FastTX) bool {
	return tx.Value == 0 && (bytes.Equal(tx.Address, COO_ADDRESS_BYTES) || bytes.Equal(tx.Address, COO_ADDRESS2_BYTES))
}

// Returns the (hash) key for a specific milestone.
// if acceptNearest is set, the nearest, more recent milestone is returned, if the other is not found.
func GetMilestoneKeyByIndex(index int, acceptNearest bool) []byte {
	var milestoneKey []byte

	milestone := GetLatestMilestone()
	currentIndex := milestone.Index + 1
	latestIndex := 0

	db.Singleton.View(func(dbTx db.Transaction) error {
		return coding.ForPrefixInt(dbTx, ns.Prefix(ns.NamespaceMilestone), true, func(key []byte, msIndex int) (bool, error) {
			if msIndex == index {
				milestoneKey = ns.Key(key, ns.NamespaceHash)
				return false, nil
			} else if acceptNearest && (msIndex < currentIndex) && (msIndex > latestIndex) {
				// TODO: search for nearest distance better?
				milestoneKey = ns.Key(key, ns.NamespaceHash)
				latestIndex = msIndex
			}
			return true, nil
		})
	})

	return milestoneKey
}
