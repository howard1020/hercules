package tangle

import (
	"sync"
	"time"

	"../logs"
	"../server"
	"../snapshot"
)

var (
	tangleReportTicker          *time.Ticker
	tangleReportTickerWaitGroup = &sync.WaitGroup{}
	tangleReportTickerQuit      = make(chan struct{})
)

func Report() {
	milestone := GetLatestMilestone()
	pendingRequestsCnt := len(PendingRequests)
	pendingSolidificationReqCnt := len(PendingSolidificationRequests)
	totalPendingReqCnt := pendingRequestsCnt + pendingSolidificationReqCnt

	logs.Log.Debugf("TX IN/OUT:     %v, %v", server.TotalIncTx, outgoing)
	logs.Log.Debugf("SERVER I/O Q:  %v, %v", len(srv.Incoming), len(srv.Outgoing))
	logs.Log.Infof("TRANSACTIONS:  %v, PENDING: Solidification: %v/%v, Other: %v/%v", totalTransactions, pendingSolidificationReqCnt, totalPendingReqCnt, pendingRequestsCnt, totalPendingReqCnt)
	logs.Log.Infof("CONFIRMATIONS: %v, PENDING: %v", totalConfirmations, len(pendingConfirmationsQueue))
	logs.Log.Debugf("PENDING TRIMS: %v", len(snapshot.EdgeTransactions))
	logs.Log.Infof("MILESTONES:    Current Index: %v, PENDING: %v", milestone.Index, len(pendingMilestoneQueue))
	logs.Log.Infof("TIPS:          %v", TipsCache.EntryCount())
}

func report() {
	tangleReportTickerWaitGroup.Add(1)
	defer tangleReportTickerWaitGroup.Done()

	Report()

	tangleReportTicker = time.NewTicker(reportInterval)
	for {
		select {
		case <-tangleReportTickerQuit:
			return

		case <-tangleReportTicker.C:
			if ended {
				break
			}
			Report()
		}
	}
}
