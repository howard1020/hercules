package tangle

import (
	"bytes"
	"sync"
	"time"

	"../db"
	"../db/coding"
	"../db/ns"
	"../logs"
	"../server"
	"../transaction"
	"../utils"
)

const (
	reRequestInterval = time.Duration(10) * time.Second
	maxTimesRequest   = 10
	maxAbandonedCnt   = 10
)

var (
	PendingRequests                   = make(map[string]int) // Tx Hash of the pending request -> AbandonedCnt (how much Neighbors have no answer)
	PendingRequestsLock               = &sync.RWMutex{}
	PendingSolidificationRequests     = make(map[string]int) // Tx Hash of the pending request to make a milestone solid
	PendingSolidificationRequestsLock = &sync.RWMutex{}
	outgoingRunnerTicker              *time.Ticker
	outgoingRunnerWaitGroup           = &sync.WaitGroup{}
	outgoingRunnerTickerQuit          = make(chan struct{})
	outgoingInterval                  = time.Duration(2) * time.Millisecond
)

func Broadcast(data []byte, excludeNeighbor *server.Neighbor) int {
	sent := 0

	server.NeighborsLock.RLock()
	for _, neighbor := range server.Neighbors {
		if neighbor == excludeNeighbor {
			continue
		}

		sendMessageToNeighbor(neighbor, data, nil, false) // Transmit the data to the neighbor and process our pending requests
		sent++
	}
	server.NeighborsLock.RUnlock()

	return sent
}

func pendingOnLoad() {
	loadPendingRequestsFromDB()

	if lowEndDevice {
		outgoingInterval *= 5
	}
}

func loadPendingRequestsFromDB() {
	logs.Log.Info("Loading pending requests")

	pending := 0
	solidification := 0

	var keysToRemove [][]byte

	server.NeighborsLock.RLock()
	for _, neighbor := range server.Neighbors {
		neighbor.QueuesInitialized = true
	}

	err := db.Singleton.View(func(dbTx db.Transaction) error {
		err := ns.ForNamespace(dbTx, ns.NamespacePendingHash, true, func(key, hash []byte) (bool, error) {

			if dbTx.HasKey(ns.Key(key, ns.NamespaceHash)) || dbTx.HasKey(ns.Key(key, ns.NamespacePendingSolidification)) {
				// Tx already exists in the DB or is a solidification request => remove the "pending keys"
				keysToRemove = append(keysToRemove, key)
				return true, nil
			}

			abandonedCnt, err := coding.GetInt(dbTx, ns.Key(key, ns.NamespacePendingAbandonedCount))
			if err != nil {
				abandonedCnt = 0
			}

			addPendingRequest(hash, false, abandonedCnt, false, nil)
			pending++

			return true, nil
		})
		if err != nil {
			return err
		}

		return ns.ForNamespace(dbTx, ns.NamespacePendingSolidification, true, func(key, hash []byte) (bool, error) {

			if dbTx.HasKey(ns.Key(key, ns.NamespaceHash)) {
				// Tx already exists in the DB => remove the "pending keys"
				keysToRemove = append(keysToRemove, key)
				return true, nil
			}

			addPendingRequest(hash, false, 0, true, nil)
			solidification++

			return true, nil
		})
	})
	if err != nil {
		logs.Log.Panicf("Load pending requests failed! Err:", err)
	}

	// Wait with the unlock until all requests are loaded
	server.NeighborsLock.RUnlock()

	db.Singleton.Update(func(dbTx db.Transaction) (err error) {
		for _, key := range keysToRemove {
			dbTx.Remove(key) // NamespacePendingHash or NamespacePendingSolidification
			dbTx.Remove(ns.Key(key, ns.NamespacePendingAbandonedCount))
			dbTx.Remove(ns.Key(key, ns.NamespacePendingConfirmed))
		}
		return nil
	})

	total := solidification + pending
	logs.Log.Infof("Pending requests loaded! Solidification: %v/%v, Other: %v/%v", solidification, total, pending, total)
}

func storePendingRequestCount() {
	logs.Log.Debug("Store pending requests in database")

	PendingRequestsLock.RLock()
	err := db.Singleton.Update(func(dbTx db.Transaction) (err error) {
		for txHash, abandonedCnt := range PendingRequests {
			key := ns.HashKey([]byte(txHash), ns.NamespacePendingAbandonedCount)
			err := coding.PutInt(dbTx, key, abandonedCnt)
			if err == db.ErrTransactionTooBig {
				err = dbTx.Commit()
				if err != nil {
					return err
				}
				dbTx = db.Singleton.NewTransaction(true)
				err = coding.PutInt(dbTx, key, abandonedCnt)
				if err != nil {
					return err
				}
			}
		}
		return nil
	})
	PendingRequestsLock.RUnlock()
	if err != nil {
		logs.Log.Panicf("Store pending requests in database failed! Err:", err)
	}
}

/*
How do pending requests work?

If there is a need for a pending request, e.g. we received a request for an unknown hash or we don't know branch/trunk of an tx:

1. Check if it is not stored in DB yet => requestIfMissing
2. Add the request to the PendingRequests and store the NamespacePendingHash in DB to mark this request as pending => addPendingRequest
3. Put the pending request in a channel for every existing neighbor
	If a new neighbor is added, all the PendingRequests are added to the queues => fillPendingRequestChannelsOfNeighbor
4. The neighbors read from that channel and check the following:
    - Is the request still in PendingRequests? Otherwise confirmed => drop from queue
    - If not confirmed yet, always re-add it to the own queue
    - Did the Neighbor waited long enough to request it again?
    - If yes, request it, set new time of request for this neighbor and increment RequestCount in PendingRequestsCache
	- If a request is requested to often for a neighbor, the request is marked as abondoned => drop the request in this neighbor and increment a counter
	- If the abandoned counter gets to high (too much neighbors don't know that request) => drop the whole request from PendingRequests
5. If the request is received, drop the hash from the PendingRequests, but do not remove the markers in the DB.
   The markers get removed at next startup, because at loading of a request there is a check if the Tx hash already exists in the DB.
6. Store the RequestCounter for every pending request in the DB at shutdown

- There is a second map for requests to get the Subtangle solid => PendingSolidificationRequests
- This map / queue is processed with higher priority.
- If a request is added to that map, the same request is deleted from PendingRequests map.
- A request in that map will never get abandonded
*/
func getSomeRequestByNeighbor(neighbor *server.Neighbor, isSolidification bool) []byte {

	if neighbor == nil {
		return nil
	}

	if !neighbor.QueuesInitialized {
		fillPendingRequestChannelsOfNeighbor(neighbor)
	}

	var reqHash []byte
	if isSolidification {
		reqHash = getPendingRequestFromQueueByNeighbor(neighbor, neighbor.SolidificationRequestQueue, &PendingSolidificationRequests, PendingSolidificationRequestsLock, isSolidification)
		if reqHash != nil {
			// Requested Milestone hash found
			return reqHash
		}
	}

	reqHash = getPendingRequestFromQueueByNeighbor(neighbor, neighbor.TipRequestQueue, &PendingRequests, PendingRequestsLock, isSolidification)
	return reqHash
}

func fillPendingRequestChannelsOfNeighbor(neighbor *server.Neighbor) {

	PendingRequestsLock.RLock()
	for key := range PendingRequests {
		neighbor.TipRequestQueue <- &server.Request{Hash: key, LastReqTime: time.Now().Add(-reRequestInterval), RequestCnt: 0}
	}
	PendingRequestsLock.RUnlock()

	PendingSolidificationRequestsLock.RLock()
	for key := range PendingSolidificationRequests {
		neighbor.SolidificationRequestQueue <- &server.Request{Hash: key, LastReqTime: time.Now().Add(-reRequestInterval), RequestCnt: 0}
	}
	PendingSolidificationRequestsLock.RUnlock()

	neighbor.QueuesInitialized = true
}

func getPendingRequestFromQueueByNeighbor(neighbor *server.Neighbor, queue chan *server.Request, pendingMap *map[string]int, pendingLock *sync.RWMutex, isSolidification bool) []byte {
	seenReqHashes := make(map[string]*struct{})

	for {
		select {
		case queuedReq := <-queue:
			hash := queuedReq.Hash

			_, seenAlready := seenReqHashes[hash]
			if seenAlready {
				// We are back at the start of the queue => Nothing pending so far
				// Re-add it to the queue (TODO non-blocking write?)
				queue <- queuedReq
				return nil
			}
			seenReqHashes[hash] = nil // mark queuedRequest as seen

			pendingLock.RLock()
			abandonedCnt, exists := (*pendingMap)[hash]
			pendingLock.RUnlock()

			if !exists {
				// Request not pending anymore => don't add it back to the queue and continue to search
				continue
			}

			if !isSolidification && (queuedReq.RequestCnt > maxTimesRequest) {
				// Requested to often for this neighbor => don't add it back to the queue and continue to search
				// Solidification requests are not deleted!
				abandonedCnt++

				pendingLock.Lock()
				if abandonedCnt > maxAbandonedCnt {
					// Request is abandoned by too much neighbors => Remove if from the PendingRequests
					// This is probably a fake or invalid spam referenced by trunk/branch of a transaction. Just ignore.
					delete((*pendingMap), hash)
				} else {
					// Mark the request as abandoned by this neighbor
					(*pendingMap)[hash] = abandonedCnt
				}
				pendingLock.Unlock()

				continue
			}

			if time.Now().Sub(queuedReq.LastReqTime) < reRequestInterval {
				// Time before next request is not over yet => continue to search
				// Re-add it to the queue (TODO non-blocking write?)
				queue <- queuedReq
				continue
			}

			// We found a valid request

			// Check that the neighbor is connected as it is sending transactions.
			// Only if it is a good neighbor, set this request as sent.
			if time.Now().Sub(neighbor.LastIncomingTime) < reRequestInterval {
				// => increase request count
				queuedReq.RequestCnt++
			}

			// Mark the tx as newly requested
			queuedReq.LastReqTime = time.Now()

			// Re-add it to the queue (TODO non-blocking write?)
			queue <- queuedReq

			return []byte(hash)

		default:
			// Non blocking read is essential here, otherwise it causes a deadlock if the queue is empty
			return nil
		}
	}
}

func outgoingRunner() {
	outgoingRunnerWaitGroup.Add(1)
	defer outgoingRunnerWaitGroup.Done()

	outgoingRunnerTicker = time.NewTicker(outgoingInterval)
	for {
		select {
		case <-outgoingRunnerTickerQuit:
			return

		case <-outgoingRunnerTicker.C:
			if ended {
				break
			}
			executeOutgoingRunner()
		}
	}
}

func executeOutgoingRunner() {

	server.NeighborsLock.RLock()
	for _, neighbor := range server.Neighbors {
		sendMessageToNeighbor(neighbor, nil, nil, true) // Send Tip or latest Milestone and process our pending requests
	}
	server.NeighborsLock.RUnlock()
}

// Creates a message and sends it to the neighbor
func sendMessageToNeighbor(neighbor *server.Neighbor, data []byte, reqHash []byte, sendTip bool) {

	var replyHash []byte
	var replyBytes []byte

	if data == nil {
		// No data to send was given => e.g. by Broadcast()

		if sendTip {
			if utils.Random(0, 100) < P_TIP_REPLY {
				if utils.Random(0, 100) < P_SEND_MILESTONE {
					milestone := GetLatestMilestone()
					if (milestone.TX != nil) && (milestone.TX != transaction.TipFastTX) {
						// Send the latest milestone
						sendTip = false
						replyHash = milestone.TX.Hash
						replyBytes = milestone.TX.Bytes
					}
				}

				if sendTip {
					// Get a random tip (TODO: first solid, then non solid)
					replyHash, replyBytes = getRandomTip(nil, neighbor)
				}
			} else {
				// Do not answer to all tip requests
				return
			}
		} else {
			reqBytes, err := db.Singleton.GetBytes(ns.HashKey(reqHash, ns.NamespaceBytes))
			if err == nil {
				// Requested Tx was found in our database
				replyHash = reqHash
				replyBytes = reqBytes
			} else {
				// Request from the neighbor is an actual tx and missing in our DB, so add it to the pending queue
				if utils.Random(0, 100) < P_PROPAGATE_REQUEST {
					addPendingRequest(reqHash, true, 0, false, nil)
				}
			}
		}
	} else {
		// Data to send was already given => e.g. by Broadcast()
		replyBytes = data
	}

	ourReqHash := getSomeRequestByNeighbor(neighbor, utils.Random(0, 100) < P_SELECT_MILESTONE_CHILD)

	if (ourReqHash == nil) && (replyBytes == nil) {
		// If we don't have what the neighbor asks for and we don't have anything to ask for, don't reply
		return
	}

	if replyBytes == nil {
		// If we don't have what the neighbor asks for, but have something to ask, then ask.
		replyBytes = transaction.TipBytes[:DATA_SIZE]
	}

	if ourReqHash == nil {
		// If we don't have anything to ask for, but have what the neighbor asks for, then reply.
		ourReqHash = replyHash
	}

	if ourReqHash == nil {
		// If it's still void, send empty reqHash
		ourReqHash = transaction.TipBytes[:HASH_SIZE]
	}

	sendReply(&Message{Bytes: &replyBytes, Requested: &ourReqHash, Neighbor: neighbor})
}

func sendReply(msg *Message) {
	if msg == nil {
		return
	}

	if len(*msg.Bytes) < DATA_SIZE {
		logs.Log.Errorf("Reply data doesn't contain enough bytes: %v", len(*msg.Bytes))
		return
	}
	if len(*msg.Requested) < REQ_HASH_SIZE {
		logs.Log.Errorf("Reply Hash doesn't contain enough bytes: %v", len(*msg.Requested))
		return
	}

	data := append((*msg.Bytes)[:DATA_SIZE], (*msg.Requested)[:REQ_HASH_SIZE]...)

	select {
	case srv.Outgoing <- &server.Message{Neighbor: msg.Neighbor, Msg: data}:
		outgoing++
	default: // Drop message if Outgoing Queue is full to prevent deadlocks
	}
}

func requestIfMissing(hash []byte, isSolidification bool, dbTx db.Transaction) (exists bool) {

	if bytes.Equal(hash, transaction.TipFastTX.Hash) {
		// Empty Hash
		return true
	}

	if !dbTx.HasKey(ns.HashKey(hash, ns.NamespaceHash)) {
		// Tx is not stored in the DB yet
		return addPendingRequest(hash, true, 0, isSolidification, dbTx)
	}
	return true
}

func addPendingRequest(hash []byte, storeInDB bool, abandonedCnt int, isSolidification bool, dbTx db.Transaction) (exists bool) {

	key := string(hash)

	pendingLock := PendingRequestsLock
	pendingMap := PendingRequests
	dbNamespace := ns.NamespacePendingHash

	pendingLock.RLock()
	_, exists = pendingMap[key]
	pendingLock.RUnlock()

	if exists {
		if !isSolidification {
			return exists
		} else {
			// If it is a solidification request, it should be removed in the PendingRequests
			pendingLock.Lock()
			delete(pendingMap, key)
			pendingLock.Unlock()
		}
	}

	if isSolidification {
		pendingLock = PendingSolidificationRequestsLock
		pendingMap = PendingSolidificationRequests
		dbNamespace = ns.NamespacePendingSolidification

		pendingLock.RLock()
		_, exists = pendingMap[key]
		pendingLock.RUnlock()

		if exists {
			return exists
		}
	}

	if storeInDB {
		keyPending := ns.HashKey(hash, dbNamespace)
		if dbTx == nil {
			dbTx = db.Singleton.NewTransaction(true)
			defer dbTx.Commit()
		}
		dbTx.PutBytes(keyPending, hash)
	}

	pendingLock.Lock()
	pendingMap[key] = abandonedCnt
	pendingLock.Unlock()

	server.NeighborsLock.RLock()
	for _, neighbor := range server.Neighbors {
		if isSolidification {
			neighbor.SolidificationRequestQueue <- &server.Request{Hash: key, LastReqTime: time.Now().Add(-reRequestInterval), RequestCnt: 0}
		} else {
			neighbor.TipRequestQueue <- &server.Request{Hash: key, LastReqTime: time.Now().Add(-reRequestInterval), RequestCnt: 0}
		}
	}
	server.NeighborsLock.RUnlock()

	return false
}

func removePendingRequest(hash []byte) bool {

	key := string(hash)

	existsPending := removePendingRequestFromMap(key, &PendingRequests, PendingRequestsLock)
	existsPendingSolidification := removePendingRequestFromMap(key, &PendingSolidificationRequests, PendingSolidificationRequestsLock)

	return (existsPending || existsPendingSolidification)
}

func removePendingRequestFromMap(key string, pendingMap *map[string]int, pendingLock *sync.RWMutex) (exists bool) {

	pendingLock.RLock()
	_, exists = (*pendingMap)[key]
	pendingLock.RUnlock()

	if exists {
		pendingLock.Lock()
		delete(*pendingMap, key)
		pendingLock.Unlock()
	}
	return exists
}
