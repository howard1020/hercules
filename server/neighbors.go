package server

import (
	"encoding/json"
	"errors"
	"fmt"
	"net"
	"strings"
	"sync/atomic"
	"time"

	"../config"
	"../logs"
	"../utils"
	"../utils/network"
)

const (
	maxQueueSize = 1000000
)

type BasicNodeInfo struct {
	ConnectionType string
	Identifier     string
	Port           string
	APIInfo        *APIInfo
}

type APIInfo struct {
	APIEndPoint           string
	APPName               string
	APPVersion            string
	LastSnapshotTimeStamp int64
}

type Request struct {
	Hash        string
	LastReqTime time.Time
	RequestCnt  int
}

type Neighbor struct {
	Hostname                   string // Formatted like: <domainname> (Empty if its IP address)
	Addr                       string // Formatted like: <ip>:<port> OR <domainname>:<port>
	IP                         string // Formatted like: XXX.XXX.XXX.XXX (IPv4) OR [x:x:x:...] (IPv6)
	Port                       string // Also saved separately from Addr for performance reasons
	IPAddressWithPort          string // Formatted like: XXX.XXX.XXX.XXX:x (IPv4) OR [x:x:x:...]:x (IPv6)
	UDPAddr                    *net.UDPAddr
	Incoming                   int32
	New                        int32
	PreSnapOrFuture            int32
	Invalid                    int32
	ConnectionType             string // Formatted like: udp
	PreferIPv6                 bool
	KnownIPs                   []*networkutils.IPAddress
	LastIncomingTime           time.Time
	APIInfo                    *APIInfo
	TipRequestQueue            chan *Request
	SolidificationRequestQueue chan *Request
	QueuesInitialized          bool // Marker which indicates if queues are filled after creation of the neighbor
}

func (nb *Neighbor) Write(msg *Message) {
	if ended {
		return
	}
	_, err := connection.WriteTo(msg.Msg[0:], nb.UDPAddr)
	if err != nil {
		if !ended { // Check again
			logs.Log.Errorf("Error sending message to neighbor '%v': %v", nb.Addr, err)
		}
	} else {
		TrackOutTx(1)
	}
}

func (nb *Neighbor) GetPreferredIP() string {
	return networkutils.GetPreferredIP(nb.KnownIPs, nb.PreferIPv6)
}

func (nb *Neighbor) UpdateIPAddressWithPort(ipAddressWithPort string) (changed bool) {
	if nb.IPAddressWithPort != ipAddressWithPort {
		executeForAllKnownIPs(nb, func(knownIP *networkutils.IPAddress, knownIPWithPort string) {
			if knownIPWithPort == ipAddressWithPort {
				logs.Log.Debugf("Updated IP address for '%v' from '%v' to '%v'", nb.Hostname, nb.IPAddressWithPort, ipAddressWithPort)

				NeighborsLock.Lock()
				delete(Neighbors, nb.IPAddressWithPort)
				nb.IPAddressWithPort = ipAddressWithPort
				Neighbors[nb.IPAddressWithPort] = nb
				NeighborsLock.Unlock()

				nb.PreferIPv6 = knownIP.IsIPv6()
				nb.IP = knownIP.ToString()
				nb.UDPAddr, _ = net.ResolveUDPAddr("udp", ipAddressWithPort)
				changed = true
				return
			}
		})
	}
	return changed
}

func (nb *Neighbor) UpdateAPIInfo() {
	isAPIConfigured := nb.APIInfo.APIEndPoint != ""
	if !isAPIConfigured {
		return
	}

	if nb.APIInfo.APPVersion == "" {
		nb.updateIRIAPIInfo()
	}

	if nb.IsHercules() {
		nb.updateHerculesAPIInfo()
	}
}

func (nb *Neighbor) updateIRIAPIInfo() {
	request := InterNodeRequest{Command: "getNodeInfo"}
	responseBytes, err := sendRequest(request, nb.APIInfo.APIEndPoint)
	if err != nil {
		return
	}
	var getNodeInfoResponse *GetNodeInfoResponse
	json.Unmarshal(responseBytes, &getNodeInfoResponse)

	nb.APIInfo.APPName = getNodeInfoResponse.AppName
	nb.APIInfo.APPVersion = getNodeInfoResponse.AppVersion
	logs.Log.Debugf("Updated neighbor's API info: (AppName: %s - AppVersion: %s)", nb.APIInfo.APPName, nb.APIInfo.APPVersion)
}

func (nb *Neighbor) IsHercules() bool {
	return strings.Contains(nb.APIInfo.APPName, "Hercules")
}

func (nb *Neighbor) updateHerculesAPIInfo() {
	nb.UpdateLastSnapshotTime()
}

func (nb *Neighbor) UpdateLastSnapshotTime() {
	request := InterNodeRequest{Command: "getLatestSnapshotInfo"}
	responseBytes, err := sendRequest(request, nb.APIInfo.APIEndPoint+"/snapshots")
	if err != nil {
		return
	}
	var getLatestSnapshotInfoResponse *GetLatestSnapshotInfoResponse
	json.Unmarshal(responseBytes, &getLatestSnapshotInfoResponse)

	nb.APIInfo.LastSnapshotTimeStamp = getLatestSnapshotInfoResponse.LatestSnapshot.Timestamp
	if nb.APIInfo.LastSnapshotTimeStamp > 0 {
		humanReadableSnapTime := utils.GetHumanReadableTime(nb.APIInfo.LastSnapshotTimeStamp)
		logs.Log.Debugf("Updated last snapshot time for '%s'. New time: %s", nb.Addr, humanReadableSnapTime)
	}
}

func (nb *Neighbor) TrackIncoming(cnt int32) {
	atomic.AddInt32(&nb.Incoming, cnt)
}

func (nb *Neighbor) TrackNew(cnt int32) {
	atomic.AddInt32(&nb.New, cnt)
}

func (nb *Neighbor) TrackPreSnapOrFuture(cnt int32) {
	atomic.AddInt32(&nb.PreSnapOrFuture, cnt)
}

func (nb *Neighbor) TrackInvalid(cnt int32) {
	atomic.AddInt32(&nb.Invalid, cnt)
}

func AddNeighbor(address string) error {
	neighbor, err := createNeighbor(address)

	if err != nil {
		return err
	}

	neighborExists, _ := checkNeighbourExists(neighbor)
	if neighborExists {
		return errors.New("Neighbor already exists")
	}

	NeighborsLock.Lock()
	Neighbors[neighbor.IPAddressWithPort] = neighbor
	neighbor.UpdateKnownIps()
	NeighborsLock.Unlock()

	logAddNeighbor(neighbor)

	return nil
}

func (nb *Neighbor) UpdateKnownIps() {
	executeForAllKnownIPs(nb, func(knownIP *networkutils.IPAddress, knownIPWithPort string) { knownAddresses[knownIPWithPort] = nb })
}

func (nb *Neighbor) RemoveKnownIps() {
	executeForAllKnownIPs(nb, func(knownIP *networkutils.IPAddress, knownIPWithPort string) { delete(knownAddresses, knownIPWithPort) })
}

func executeForAllKnownIPs(neighbor *Neighbor, executableFunc func(knownIP *networkutils.IPAddress, knownIPWithPort string)) {
	for _, knownIP := range neighbor.KnownIPs {
		knownIPWithPort := networkutils.GetFormattedAddress(knownIP.ToString(), neighbor.Port)
		executableFunc(knownIP, knownIPWithPort)
	}
}

func logAddNeighbor(neighbor *Neighbor) {
	addingLogMessage := "Adding neighbor '%v://%v'"
	if neighbor.Hostname != "" {
		addingLogMessage += " - IP Address: '%v'"
		logs.Log.Debugf(addingLogMessage, neighbor.ConnectionType, neighbor.Addr, neighbor.IP)
	} else {
		logs.Log.Debugf(addingLogMessage, neighbor.ConnectionType, neighbor.Addr)
	}
}

func RemoveNeighbor(address string) error {
	neighborExists, neighbor := checkNeighbourExistsByAddress(address)

	if neighborExists {
		NeighborsLock.Lock()
		neighbor.RemoveKnownIps()
		delete(Neighbors, neighbor.IPAddressWithPort)
		NeighborsLock.Unlock()
		return nil
	}

	return errors.New("Neighbor not found")
}

func GetNeighborByAddress(address string) *Neighbor {
	NeighborsLock.RLock()
	defer NeighborsLock.RUnlock()

	_, neighbor := checkNeighbourExistsByAddress(address)
	return neighbor
}

func GetNeighborByIPAddressWithPort(ipAddressWithPort string) *Neighbor {
	_, neighbor := CheckNeighbourExistsByIPAddressWithPort(ipAddressWithPort)
	return neighbor
}

func UpdateHostnameAndAPIInfo() {
	addressesToAdd, addressesToRemove, neighborsToAdd, neighborsToRemove := getHostnameUpdateInfo()

	NeighborsLock.Lock()
	for addressToRemove := range addressesToRemove {
		delete(knownAddresses, addressToRemove)
	}
	for _, address := range neighborsToRemove {
		delete(Neighbors, address)
	}
	for addressToAdd, neighbor := range addressesToAdd {
		knownAddresses[addressToAdd] = neighbor
	}
	for _, neighbor := range neighborsToAdd {
		Neighbors[neighbor.IPAddressWithPort] = neighbor
	}
	NeighborsLock.Unlock()

	NeighborsLock.RLock()
	for _, neighbor := range Neighbors {
		neighbor.UpdateAPIInfo()
	}
	NeighborsLock.RUnlock()
}

func getHostnameUpdateInfo() (addressesToAdd map[string]*Neighbor, addressesToRemove map[string]*struct{},
	neighborsToAdd []*Neighbor, neighborsToRemove []string) {

	addressesToAdd = make(map[string]*Neighbor)
	addressesToRemove = make(map[string]*struct{})

	NeighborsLock.RLock()
	defer NeighborsLock.RUnlock()

	for _, neighbor := range Neighbors {
		isRegisteredWithHostname := len(neighbor.Hostname) > 0
		if isRegisteredWithHostname {

			// Collect known addresses

			executeForAllKnownIPs(neighbor, func(knownIP *networkutils.IPAddress, knownIPWithPort string) {
				addressesToRemove[knownIPWithPort] = nil
			})

			identifier, _ := getIdentifierAndPort(neighbor.Addr)
			neighbor.KnownIPs, _, _ = getIPsAndHostname(identifier)

			// Check if new addresses changed
			executeForAllKnownIPs(neighbor, func(knownIP *networkutils.IPAddress, newIPAddressWithPort string) {
				_, exists := addressesToRemove[newIPAddressWithPort]
				if exists {
					// New address didn't changed, no need to remove it
					delete(addressesToRemove, newIPAddressWithPort)
				} else {
					// New one => add it and remove the old one
					addressesToAdd[newIPAddressWithPort] = neighbor
				}
			})

			ip := neighbor.GetPreferredIP()
			ipAddressWithPort := networkutils.GetFormattedAddress(ip, neighbor.Port)

			if neighbor.IPAddressWithPort == ipAddressWithPort {
				logs.Log.Debugf("IP address for '%v' is up-to-date ('%v')", neighbor.Hostname, neighbor.IPAddressWithPort)
			} else {
				neighbor.UDPAddr, _ = net.ResolveUDPAddr("udp", ipAddressWithPort)
				neighborsToRemove = append(neighborsToRemove, neighbor.IPAddressWithPort)

				logs.Log.Debugf("Updated IP address for '%v' from '%v' to '%v'", neighbor.Hostname, neighbor.IPAddressWithPort, ipAddressWithPort)

				neighbor.IP = ip
				neighbor.IPAddressWithPort = ipAddressWithPort
				neighborsToAdd = append(neighborsToAdd, neighbor)
			}
		}
	}
	return
}

func createNeighbor(address string) (*Neighbor, error) {
	basicNodeInfo, err := getBasicNodeInfo(address)
	if err != nil {
		return nil, err
	}

	if basicNodeInfo.ConnectionType != UDP {
		return nil, errors.New("This protocol is not supported yet")
	}

	ips, hostname, err := getIPsAndHostname(basicNodeInfo.Identifier)
	if err != nil {
		return nil, err
	}

	ip := networkutils.GetPreferredIP(ips, false)

	neighbor := Neighbor{
		Hostname:                   hostname,
		IP:                         ip,
		Addr:                       networkutils.GetFormattedAddress(basicNodeInfo.Identifier, basicNodeInfo.Port),
		Port:                       basicNodeInfo.Port,
		IPAddressWithPort:          networkutils.GetFormattedAddress(ip, basicNodeInfo.Port),
		ConnectionType:             basicNodeInfo.ConnectionType,
		Incoming:                   0,
		New:                        0,
		PreSnapOrFuture:            0,
		Invalid:                    0,
		PreferIPv6:                 false,
		KnownIPs:                   ips,
		APIInfo:                    basicNodeInfo.APIInfo,
		TipRequestQueue:            make(chan *Request, maxQueueSize),
		SolidificationRequestQueue: make(chan *Request, maxQueueSize),
	}

	if neighbor.ConnectionType == UDP {
		neighbor.UDPAddr, err = net.ResolveUDPAddr(neighbor.ConnectionType, neighbor.IPAddressWithPort)
		if err != nil {
			return nil, err
		}
	}

	neighbor.UpdateAPIInfo()

	return &neighbor, nil
}

func getBasicNodeInfo(address string) (*BasicNodeInfo, error) {
	nodeAddress, apiProtocol, apiPort := getURIAndAPIInfo(address)
	addressWithoutConnectionType, connectionType := getConnectionType(nodeAddress)
	identifier, port := getIdentifierAndPort(addressWithoutConnectionType)

	if connectionType == "" || identifier == "" || port == "" {
		return nil, errors.New("Address could not be loaded")
	}
	basicNodeInfo := &BasicNodeInfo{
		ConnectionType: connectionType,
		Identifier:     identifier,
		Port:           port,
		APIInfo:        &APIInfo{},
	}
	if apiProtocol != "" && apiPort != "" {
		basicNodeInfo.APIInfo.APIEndPoint = fmt.Sprintf("%s://%s:%s", apiProtocol, basicNodeInfo.Identifier, apiPort)
	}

	return basicNodeInfo, nil
}

func getConnectionType(address string) (addressWithoutConnectionType string, connectionType string) {
	tokens := strings.Split(address, "://")
	addressAndPortIndex := len(tokens) - 1
	if addressAndPortIndex > 0 {
		connectionType = tokens[0]
		addressWithoutConnectionType = tokens[addressAndPortIndex]
	} else {
		connectionType = UDP // default if none is provided
		addressWithoutConnectionType = address
	}
	return
}

func getIdentifierAndPort(address string) (identifier string, port string) {
	tokens := strings.Split(address, ":")
	portIndex := len(tokens) - 1
	if portIndex > 0 {
		identifier = strings.Join(tokens[:portIndex], ":")
		port = tokens[portIndex]
	} else {
		identifier = address
		port = config.AppConfig.GetString("node.port") // Tries to use same port as this node
	}

	return identifier, port
}

func getIPsAndHostname(identifier string) (ips []*networkutils.IPAddress, hostname string, err error) {
	addr := net.ParseIP(identifier)
	isIPFormat := addr != nil
	if isIPFormat {
		ipAddress := networkutils.IPAddress(addr.String())    //addr.String()
		return []*networkutils.IPAddress{&ipAddress}, "", nil // leave hostname empty when its in IP format
	}

	// Probably domain name. Check it
	addresses, err := net.LookupHost(identifier)
	if err != nil {
		return nil, "", errors.New("Could not process look up for " + identifier)
	}

	addressFound := len(addresses) > 0
	if addressFound {
		for _, addr := range addresses {
			ipAddress := networkutils.IPAddress(addr)
			ips = append(ips, &ipAddress)
		}

		return ips, identifier, nil
	}

	return nil, "", errors.New("Could not resolve a hostname for " + identifier)
}

func checkNeighbourExistsByAddress(address string) (neighborExists bool, neighbor *Neighbor) {
	NeighborsLock.RLock()
	defer NeighborsLock.RUnlock()

	basicNodeInfo, _ := getBasicNodeInfo(address)
	formattedAddress := networkutils.GetFormattedAddress(basicNodeInfo.Identifier, basicNodeInfo.Port)

	for _, candidateNeighbor := range Neighbors {
		if candidateNeighbor.Addr == formattedAddress {
			return true, candidateNeighbor
		}
	}

	return false, nil
}

func CheckNeighbourExistsByIPAddressWithPort(ipAddressWithPort string) (neighborExists bool, neighbor *Neighbor) {
	NeighborsLock.RLock()
	neighbor, neighborExists = knownAddresses[ipAddressWithPort]
	NeighborsLock.RUnlock()

	if neighborExists && neighbor.IPAddressWithPort != ipAddressWithPort {
		// If the neighbor was found, but the preferred IP is wrong => Update it!
		neighbor.UpdateIPAddressWithPort(ipAddressWithPort)
	}

	return neighborExists, neighbor
}

func checkNeighbourExists(neighbor *Neighbor) (bool, *Neighbor) {
	NeighborsLock.RLock()
	defer NeighborsLock.RUnlock()

	for _, candidateNeighbor := range Neighbors {
		if candidateNeighbor.Addr == neighbor.Addr {
			return true, candidateNeighbor
		}
	}
	return false, nil
}

func getURIAndAPIInfo(candidateURI string) (nodeAddress string, apiProtocol string, apiPort string) {
	addressSlice := strings.Split(candidateURI, "?")
	if addressSlice == nil || len(addressSlice) == 0 || addressSlice[0] == "" {
		return
	}

	nodeAddress = strings.TrimSuffix(addressSlice[0], "/")
	hasExtensions := len(addressSlice) > 1
	if hasExtensions {
		extensionSlice := strings.Split(addressSlice[1], "&")
		for _, parameter := range extensionSlice {
			parameterSlice := strings.Split(parameter, "=")
			if parameterSlice == nil || len(parameterSlice) != 2 {
				continue
			}

			parameterKey := strings.ToLower(parameterSlice[0])
			parameterValue := strings.ToLower(parameterSlice[1])
			switch parameterKey {
			case "apiprotocol":
				apiProtocol = parameterValue
			case "apiport":
				apiPort = parameterValue
			}
		}
	}
	return
}
