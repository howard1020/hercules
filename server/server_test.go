package server

import (
	"testing"
)

func TestTracking(t *testing.T) {
	TrackIncoming(1)
	if IncTxPerSec != 1 {
		t.Error("Counter is wrong!")
	}

	TrackIncoming(1)
	if IncTxPerSec != 2 {
		t.Error("Counter is wrong!")
	}

	TrackIncoming(1)
	if IncTxPerSec != 3 {
		t.Error("Counter is wrong!")
	}
}
